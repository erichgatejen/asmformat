package a8086

import (
	"erichgatejen/asmformat/base"
	"fmt"
	"strings"
)

// =====================================================================================================================
// = TYPES AND DATA

const Tab_Text = "    "
const Tab_Label = "   "

const Token_Label_Suffix = ":"
const Token_Code_Label_Prefix = "@"

type ProcessStateType int

const (
	ProcessState_Open           ProcessStateType = 0
	ProcessState_Comment        ProcessStateType = 1
	ProcessState_Directive      ProcessStateType = 2
	ProcessState_Proc           ProcessStateType = 3
	ProcessState_Proc_Directive ProcessStateType = 4
	ProcessState_Table          ProcessStateType = 5
	ProcessState_Named          ProcessStateType = 6
)

const (
	Token_IDEAL   = "IDEAL"
	Token_INCLUDE = "INCLUDE"
	Token_DOSSEG  = "DOSSEG"
	Token_MODEL   = "MODEL"
	Token_DATA    = "DATA"
	Token_FARDATA = "FARDATA"
	Token_SEGMENT = "SEGMENT"
	Token_ASSUME  = "ASSUME"
	Token_PUBLIC  = "PUBLIC"
	Token_ARG     = "ARG"
	Token_LABEL   = "LABEL"
	Token_ENDS    = "ENDS"
	Token_END     = "END"

	Token_PROC = "PROC"
	Token_ENDP = "ENDP"
)

type ProcessTokenOrderType int

const (
	ProcessTokenOrder_Process ProcessTokenOrderType = 0
	ProcessTokenOrder_PStart  ProcessTokenOrderType = 1
	ProcessTokenOrder_PEnd    ProcessTokenOrderType = 2
)

var token_process_map = map[string]ProcessTokenOrderType{
	Token_IDEAL:   ProcessTokenOrder_Process,
	Token_INCLUDE: ProcessTokenOrder_Process,
	Token_DOSSEG:  ProcessTokenOrder_Process,
	Token_MODEL:   ProcessTokenOrder_Process,
	Token_DATA:    ProcessTokenOrder_Process,
	Token_FARDATA: ProcessTokenOrder_Process,
	Token_SEGMENT: ProcessTokenOrder_Process,
	Token_ASSUME:  ProcessTokenOrder_Process,
	Token_PUBLIC:  ProcessTokenOrder_Process,
	Token_ARG:     ProcessTokenOrder_Process,
	Token_LABEL:   ProcessTokenOrder_Process,
	Token_ENDS:    ProcessTokenOrder_Process,
	Token_END:     ProcessTokenOrder_Process,
	Token_PROC:    ProcessTokenOrder_PStart,
	Token_ENDP:    ProcessTokenOrder_PEnd,
}

const (
	formatting_list_max            = 10
	formatting_minimum_token_size  = 4
	formatting_starting_token_size = 10
)

// =====================================================================================================================
// = PROCESS

func printSpaces(number int) {
	// Not elegant but gets the job done.
	for i := 1; i <= number; i++ {
		fmt.Print(" ")
	}
}

func ProcessLineTokens(line_tokens TokenList, formatting_spec [formatting_list_max]int, indent string) {
	if line_tokens.list != nil {
		if len(indent) > 0 {
			fmt.Print(indent)
		}
		for index, token := range line_tokens.list {

			fmt.Print(token.Text)

			// Don't pad the last token
			if index < (len(line_tokens.list) - 1) {
				printSpaces(formatting_spec[index] - len(token.Text))
			}

			if index < (len(line_tokens.list) - 1) {
				fmt.Print(indent)
			}
		}
	}
}

func next_is_comment(lists SourceLinesTokenLists, index int) bool {
	result := false
	find_index := index + 1
	for ; find_index < len(lists); find_index++ {
		if lists[index+1].list != nil {
			if lists[index+1].list[0].Type == TokenType_Comment {
				result = true
			}
			break
		}
	}
	return result
}

func next_is_directive(lists SourceLinesTokenLists, index int) (bool, int) {
	result := false
	find_index := index + 1
	for ; find_index < len(lists); find_index++ {
		thing := lists[find_index].list
		if thing != nil {
			_, ok := token_process_map[strings.ToUpper(lists[find_index].list[0].Text)]
			if ok {
				result = true
			}
			break
		}
	}
	return result, find_index
}

func next_is_directive_bool(lists SourceLinesTokenLists, index int) bool {
	result, _ := next_is_directive(lists, index)
	return result
}

func LookAheadFormatting(lists SourceLinesTokenLists, index int) [formatting_list_max]int {
	formatting_spec := [formatting_list_max]int{formatting_minimum_token_size, formatting_minimum_token_size,
		formatting_minimum_token_size, formatting_minimum_token_size, formatting_minimum_token_size,
		formatting_minimum_token_size, formatting_minimum_token_size, formatting_minimum_token_size,
		formatting_minimum_token_size, formatting_minimum_token_size}

	// Process until we encounter a comma or a directive.
	for spot := index + 1; spot < len(lists); spot++ {
		if lists[spot].list != nil {
			//_, ok := token_process_map[strings.ToUpper(lists[spot].list[0].Text)]
			if lists[spot].list[0].Type == TokenType_Comment ||
				token_process_map[strings.ToUpper(lists[spot].list[0].Text)] == ProcessTokenOrder_PEnd ||
				strings.HasSuffix(lists[spot].list[0].Text, Token_Label_Suffix) {
				break
			}
			for token_index, token := range lists[spot].list {
				if token.Type != TokenType_Comment {
					if len(token.Text) > formatting_spec[token_index] {
						formatting_spec[token_index] = len(token.Text)
					}
				}

			}
		}
	}

	// Make sure everything has at least a space.
	for findex := 0; findex < len(formatting_spec); findex++ {
		formatting_spec[findex] = formatting_spec[findex] + 1
	}

	return formatting_spec
}

func process_line_comment(state ProcessStateType, lists SourceLinesTokenLists, index int,
	formatting_spec [formatting_list_max]int, indent string) (ProcessStateType, [formatting_list_max]int) {
	if !next_is_comment(lists, index) {
		formatting_spec = LookAheadFormatting(lists, index)
	}
	fmt.Print(indent)
	fmt.Print(lists[index].list[0].Text)
	return state, formatting_spec
}

func process_line_pstart(state ProcessStateType, lists SourceLinesTokenLists, index int, indent string,
	formatting_spec [formatting_list_max]int) (ProcessStateType, string, [formatting_list_max]int) {
	ProcessLineTokens(lists[index], formatting_spec, indent)
	new_formatting_spec := LookAheadFormatting(lists, index)
	if next_is_directive_bool(lists, index) {
		state = ProcessState_Proc_Directive
	} else {
		state = ProcessState_Proc
	}

	return state, Tab_Text, new_formatting_spec
}

func process_line_table(lists SourceLinesTokenLists, index int, indent string, formatting_spec [formatting_list_max]int) (
	ProcessStateType, string, [formatting_list_max]int) {
	ProcessLineTokens(lists[index], formatting_spec, indent)
	new_formatting_spec := LookAheadFormatting(lists, index)
	return ProcessState_Table, Tab_Text, new_formatting_spec
}

func process_line_pend(state ProcessStateType, lists SourceLinesTokenLists, index int, indent string) (
	ProcessStateType, string, [formatting_list_max]int) {
	indent = ""
	formatting_spec := LookAheadFormatting(lists, index-1)
	ProcessLineTokens(lists[index], formatting_spec, indent)
	formatting_spec = LookAheadFormatting(lists, index)
	if next_is_directive_bool(lists, index) {
		state = ProcessState_Directive
	} else {
		state = ProcessState_Open
	}
	return state, "", formatting_spec
}

func ProcessLines(source_info SourceInfo) {

	state := ProcessState_Open
	formatting_spec := [formatting_list_max]int{formatting_starting_token_size, formatting_starting_token_size,
		formatting_starting_token_size, formatting_starting_token_size, formatting_starting_token_size,
		formatting_starting_token_size, formatting_starting_token_size, formatting_starting_token_size,
		formatting_starting_token_size, formatting_starting_token_size}
	LookAheadFormatting(source_info.Source_lines_token_lists, 0)

	indent := ""
	for index, token_list := range source_info.Source_lines_token_lists {

		// Process first token
		if len(source_info.Source_lines_token_lists) > 0 && token_list.list != nil {
			directive, is_a_directive := token_process_map[strings.ToUpper(token_list.list[0].Text)]

			switch state {
			case ProcessState_Open:
				if token_list.list[0].Type == TokenType_Comment {
					state, formatting_spec = process_line_comment(state, source_info.Source_lines_token_lists, index,
						formatting_spec, indent)
					//indent = ""

				} else if is_a_directive && directive == ProcessTokenOrder_PStart {
					state, indent, formatting_spec = process_line_pstart(state, source_info.Source_lines_token_lists,
						index, indent, formatting_spec)

				} else if is_a_directive && directive == ProcessTokenOrder_PEnd {
					// Bad case
					indent = ""
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else if !is_a_directive && strings.HasSuffix(token_list.list[0].Text, Token_Label_Suffix) {
					state, indent, formatting_spec = process_line_table(source_info.Source_lines_token_lists, index, indent, formatting_spec)

				} else if is_a_directive {
					ProcessLineTokens(token_list, formatting_spec, indent)
					state = ProcessState_Directive

				} else {
					ProcessLineTokens(token_list, formatting_spec, indent)
				}
				break

			//case ProcessState_Comment:
			case ProcessState_Directive:
				if token_list.list[0].Type == TokenType_Comment {
					state, formatting_spec = process_line_comment(ProcessState_Open, source_info.Source_lines_token_lists,
						index, formatting_spec, indent)

				} else if is_a_directive && directive == ProcessTokenOrder_PStart {
					state, indent, formatting_spec = process_line_pstart(state, source_info.Source_lines_token_lists,
						index, indent, formatting_spec)

				} else if is_a_directive && directive == ProcessTokenOrder_PEnd {
					// Bad case
					indent = ""
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else if !is_a_directive && strings.HasSuffix(token_list.list[0].Text, Token_Label_Suffix) {
					state, indent, formatting_spec = process_line_table(source_info.Source_lines_token_lists, index, indent, formatting_spec)

				} else if is_a_directive {
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else {
					formatting_spec = LookAheadFormatting(source_info.Source_lines_token_lists, index-1)
					ProcessLineTokens(token_list, formatting_spec, indent)
					state = ProcessState_Open
				}
				break

			case ProcessState_Proc:
				if token_list.list[0].Type == TokenType_Comment {
					state, formatting_spec = process_line_comment(ProcessState_Open, source_info.Source_lines_token_lists,
						index, formatting_spec, indent)

				} else if is_a_directive && directive == ProcessTokenOrder_PStart {
					// Bad case
					indent = ""
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else if is_a_directive && directive == ProcessTokenOrder_PEnd {
					state, indent, formatting_spec = process_line_pend(state, source_info.Source_lines_token_lists, index, indent)

				} else if !is_a_directive && strings.HasSuffix(token_list.list[0].Text, Token_Label_Suffix) {
					ProcessLineTokens(token_list, formatting_spec, Tab_Label)

				} else if is_a_directive {
					formatting_spec = LookAheadFormatting(source_info.Source_lines_token_lists, index-1)
					ProcessLineTokens(token_list, formatting_spec, indent)
					state = ProcessState_Proc_Directive

				} else {
					ProcessLineTokens(token_list, formatting_spec, indent)
				}
				break

			case ProcessState_Proc_Directive:
				if token_list.list[0].Type == TokenType_Comment {
					state, formatting_spec = process_line_comment(ProcessState_Open, source_info.Source_lines_token_lists,
						index, formatting_spec, indent)

				} else if is_a_directive && directive == ProcessTokenOrder_PStart {
					// Bad case
					indent = ""
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else if is_a_directive && directive == ProcessTokenOrder_PEnd {
					state, indent, formatting_spec = process_line_pend(state, source_info.Source_lines_token_lists, index, indent)

				} else if !is_a_directive && strings.HasSuffix(token_list.list[0].Text, Token_Label_Suffix) {
					ProcessLineTokens(token_list, formatting_spec, Tab_Label)

				} else if is_a_directive {
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else {
					formatting_spec = LookAheadFormatting(source_info.Source_lines_token_lists, index-1)
					ProcessLineTokens(token_list, formatting_spec, indent)
					state = ProcessState_Proc
				}
				break

			case ProcessState_Table:
				if token_list.list[0].Type == TokenType_Comment {
					state, formatting_spec = process_line_comment(ProcessState_Open, source_info.Source_lines_token_lists,
						index, formatting_spec, indent)

				} else if is_a_directive && directive == ProcessTokenOrder_PStart {
					state, indent, formatting_spec = process_line_pstart(state, source_info.Source_lines_token_lists,
						index, indent, formatting_spec)

				} else if is_a_directive && directive == ProcessTokenOrder_PEnd {
					// Bad case
					indent = ""
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else if !is_a_directive && strings.HasSuffix(token_list.list[0].Text, Token_Label_Suffix) {
					ProcessLineTokens(token_list, formatting_spec, indent)

				} else if is_a_directive {
					formatting_spec = LookAheadFormatting(source_info.Source_lines_token_lists, index-1)
					ProcessLineTokens(token_list, formatting_spec, indent)
					state = ProcessState_Proc_Directive

				} else {
					ProcessLineTokens(token_list, formatting_spec, indent)
				}
				break

			case ProcessState_Named:
				break
			}

		}
		fmt.Println()
	}

}

// =====================================================================================================================
// = ENTRY POINT

func Format(source_file_path string) error {

	err, source := base.OpenSourceFile(source_file_path)
	if err != nil {
		return err
	}
	defer source.Close()

	// Tokenize the lines.
	var source_info SourceInfo
	line := source.GetLine()
	for line != nil {
		source_info.Add(TokenizeLine(*line))
		line = source.GetLine()
	}

	// Process the lines.
	ProcessLines(source_info)

	return nil
}
