package a8086

import (
	"erichgatejen/asmformat/base"
	"unicode"
)

// =====================================================================================================================
// = TYPES AND DATA

const comment_character = ';'
const comma_character = ','
const comma_character_string = ","
const quote_character = '\''

type TokenType int

const (
	TokenType_Text    TokenType = 0
	TokenType_Comma   TokenType = 1
	TokenType_Comment TokenType = 2
)

type StateType int

const (
	State_Open    StateType = 0
	State_Text    StateType = 1
	State_Comment StateType = 2
	State_Quote   StateType = 3
)

// =====================================================================================================================
// = STRUCT

type Token struct {
	Type      TokenType
	Text      string
	start_pos int
	end_pos   int
}

func NewToken(t TokenType, tx string, start_pos int, end_pos int) *Token {
	// I'm doing this because I expect token to expand with processed information
	token := Token{t, tx, start_pos, end_pos}
	return &token
}

type TokenList struct {
	list []*Token
}

type WorkingLineData struct {
	state         StateType
	line          string
	token_list    TokenList
	current_start int
}

func NewWorkingLineData(line string) *WorkingLineData {
	line_data := WorkingLineData{}
	line_data.state = State_Open
	line_data.line = line
	line_data.current_start = 0
	return &line_data
}

func (working_line_data *WorkingLineData) CloseTextToken(start int, end int) {
	working_line_data.token_list.list = append(working_line_data.token_list.list,
		NewToken(TokenType_Text, working_line_data.line[start:end], start, end))
}

func (working_line_data *WorkingLineData) CloseCommentToken(start int, end int) {
	working_line_data.token_list.list = append(working_line_data.token_list.list,
		NewToken(TokenType_Comment, working_line_data.line[start:end], start, end))
}

func (working_line_data *WorkingLineData) merge_with_previous_token(text string) bool {
	if len(working_line_data.token_list.list) > 0 {
		spot := len(working_line_data.token_list.list) - 1
		working_line_data.token_list.list[spot].Text = working_line_data.token_list.list[spot].Text + text
		return true
	}
	return false
}

func (working_line_data *WorkingLineData) ConsumeCharacter(index int, character rune) {

	switch working_line_data.state {
	case State_Open:
		if character == comment_character {
			working_line_data.state = State_Comment
			working_line_data.current_start = index

		} else if character == comma_character {
			working_line_data.state = State_Open
			if !working_line_data.merge_with_previous_token(comma_character_string) {
				working_line_data.token_list.list = append(working_line_data.token_list.list,
					NewToken(TokenType_Comma, "", index, index+1))
			}

		} else if character == quote_character {
			working_line_data.state = State_Quote
			working_line_data.current_start = index

		} else if !unicode.IsSpace(character) {
			working_line_data.state = State_Text
			working_line_data.current_start = index
		}
		// Just eat whitespace
		break

	case State_Text:
		if character == comment_character {
			working_line_data.CloseTextToken(working_line_data.current_start, index)
			working_line_data.state = State_Comment
			working_line_data.current_start = index

		} else if character == comma_character {
			working_line_data.CloseTextToken(working_line_data.current_start, index)
			working_line_data.state = State_Open
			if !working_line_data.merge_with_previous_token(comma_character_string) {
				working_line_data.token_list.list = append(working_line_data.token_list.list,
					NewToken(TokenType_Comma, "", index, index+1))
			}

		} else if unicode.IsSpace(character) {
			working_line_data.CloseTextToken(working_line_data.current_start, index)
			working_line_data.state = State_Open
		}
		// Just eat whitespace
		break

	case State_Comment:
		// We just let it consume the rest.
		break

	case State_Quote:
		if character == quote_character {
			working_line_data.state = State_Open
			working_line_data.CloseTextToken(working_line_data.current_start, index)
		}
		break
	}

}

func (working_line_data *WorkingLineData) ConsumeDone(index int) {
	switch working_line_data.state {
	case State_Open:
		// Just dangling whitespace
		break

	case State_Text:
		working_line_data.CloseTextToken(working_line_data.current_start, index)
		break

	case State_Comment:
		working_line_data.CloseCommentToken(working_line_data.current_start, index)
		break
	}
}

type SourceLinesTokenLists = []TokenList

type SourceInfo struct {
	Source_lines_token_lists SourceLinesTokenLists
}

func (source_line_tokens *SourceInfo) Add(token_list TokenList) {
	source_line_tokens.Source_lines_token_lists = append(source_line_tokens.Source_lines_token_lists, token_list)
}

// =====================================================================================================================
// = FUNCTION

func TokenizeLine(line string) TokenList {
	line_token_list := NewWorkingLineData(line)
	base.ReadLine(line_token_list, line)
	return line_token_list.token_list
}
