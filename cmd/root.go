package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

// =====================================================================================================================
// = DATA

var (
	input_file string
)

// =====================================================================================================================
// = COMMAND

var root_cmd = &cobra.Command{
	Use:   "asmformat",
	Short: "Formatting ASM",
	Long:  `Formatting ASM.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Welcome to ASMFORMAT.\n\n")
	},
	PersistentPreRunE: preperation,
}

func init() {
	root_cmd.PersistentFlags().StringVar(&input_file, "root", ".", "root directory path")
}

func Execute() {
	if err := root_cmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

// =====================================================================================================================
// = PREPARATION

func preperation(cmd *cobra.Command, args []string) error {
	return nil
}
