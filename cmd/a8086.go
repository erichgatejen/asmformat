package cmd

import (
	a8086 "erichgatejen/asmformat/format/a8086"
	"github.com/spf13/cobra"
)

// =====================================================================================================================
// = DATA

// =====================================================================================================================
// = FUNCTIONALITY

func format8086(cmd *cobra.Command, args []string) error {
	// These could be an interface and dispatch by option, but since there is only one now
	// I'm not going to bother and directly call it.
	return a8086.Format(args[0])
}

// =====================================================================================================================
// = COBRA COMMAND - GATHER

var a8086_cmd = &cobra.Command{
	Use:   "8086 [source file]",
	Short: "Format 8086",
	RunE:  format8086,
	Args:  cobra.MinimumNArgs(1),
}

func init() {
	root_cmd.AddCommand(a8086_cmd)
}
