package main

import (
	"erichgatejen/asmformat/cmd"
)

func main() {
	cmd.Execute()
}
