package base

import (
	"bufio"
	"fmt"
	"os"
)

// =====================================================================================================================
// = SOURCE FILE LINE READER.

const source_file_scanner_buffer int = 1048576

type SourceFile struct {
	file    *os.File
	scanner *bufio.Scanner
}

func OpenSourceFile(file_path string) (error, *SourceFile) {

	source_file := SourceFile{}
	var err error

	// Open it
	source_file.file, err = os.Open(file_path)
	if err == nil {
		// new scanner and size it
		source_file.scanner = bufio.NewScanner(source_file.file)
		buf := make([]byte, source_file_scanner_buffer)
		source_file.scanner.Buffer(buf, source_file_scanner_buffer)
	}

	return err, &source_file
}

func (source_file *SourceFile) GetLine() *string {
	if source_file.scanner.Scan() {
		value := source_file.scanner.Text()
		return &value
	}

	err := source_file.scanner.Err()
	if err != nil {
		fmt.Println(err.Error())
	}
	return nil
}

func (source_file *SourceFile) Close() {
	source_file.file.Close()
}

// =====================================================================================================================
// = LINE CHARACTER READER

type ReadLineConsumer interface {
	ConsumeCharacter(int, rune)
	ConsumeDone(int)
}

func ReadLine(consumer ReadLineConsumer, line string) {

	for index, character := range line {
		consumer.ConsumeCharacter(index, character)
	}
	consumer.ConsumeDone(len(line))
}
