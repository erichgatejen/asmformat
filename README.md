# ASMFORMAT

A tool for formatting assembly code.  I did this for two reasons:  1) The formatting for the XTILE project was really bad and I didn't like any of the available tools.  2) I wanted to refresh my knowledge of GOlang.  
I have only included x86 assembler but it should work for both BASM and Borland.

The help will tell you all you need to know on how to use it.

``` ./asmformat --help
Formatting ASM.

Usage:
  asmformat [flags]
  asmformat [command]

Available Commands:
  8086        Format 8086
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command

Flags:
  -h, --help          help for asmformat
      --root string   root directory path (default ".")

Use "asmformat [command] --help" for more information about a command.```
